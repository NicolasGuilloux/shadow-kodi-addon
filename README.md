# Shadow Kodi Addon

Unofficial Kodi addon for Linux to bring [Shadow](https://shadow.tech/) inside our media center.

Right now, the addon only downloads/updates the AppImage and starts it. No integration with kodi was made yet. This is more the beginning of a project than an actual working Kodi addon, so do not expect much.

All kind of help is greatly appreciated !


## Installation

![Pipeline state](https://gitlab.com/NicolasGuilloux/shadow-kodi-addon/badges/master/build.svg)

Download the ZIP file from the pipeline: https://gitlab.com/NicolasGuilloux/shadow-kodi-addon/-/jobs/artifacts/master/raw/plugin.game.shadow-beta.zip?job=zip_build

On Kodi, allow Unknown Sources in the parameters. Go in the extension manager, select "Install from a zip file" and select the ZIP file previously downloaded.


## Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)


## Author
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")

If you want to support my work, I hate donation :) But Shadow provides another kind of "donation": the referral program. So feel free to share my referral code (NICNC9DP) or share this link: https://shop.shadow.tech/invite/NICNC9DP

[Shadow](https://shadow.tech) is a trademark of [Blade Group](http://www.blade-group.com/).
