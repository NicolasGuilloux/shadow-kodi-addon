import urllib
import json

def downloadApp(version):
    url = 'https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/' + version + '/download?job=app_build'

    file_name = pathAppImage
    u = urllib.urlopen(url)
    f = open(file_name, 'wb')
    meta = u.info()
    file_size = int(meta.getheaders("Content-Length")[0])

    # Create a window instance.
    window = pyxbmct.AddonDialogWindow('Hello, World!')

    # Set the window width, height and the grid resolution: 2 rows, 3 columns.
    window.setGeometry(350, 150, 2, 3)

    # Create a text label.
    label = pyxbmct.Label("Downloading: %s Bytes: %s" % (file_name, file_size), alignment=pyxbmct.ALIGN_CENTER)

    # Place the label on the window grid.
    window.placeControl(label, 0, 0, columnspan=3)

    # Create a button.
    button = pyxbmct.Button('Close')

    # Place the button on the window grid.
    window.placeControl(button, 1, 1)

    # Set initial focus on the button.
    window.setFocus(button)

    # Connect the button to a function.
    window.connect(button, window.close)

    # Connect a key action to a function.
    window.connect(pyxbmct.ACTION_NAV_BACK, window.close)

    # Show the created window.
    window.doModal()

    # Delete the window instance when it is no longer used.
    del window

    file_size_dl = 0
    block_sz = 8192
    while True:
        buffer = u.read(block_sz)
        if not buffer:
            break

        file_size_dl += len(buffer)
        f.write(buffer)
        status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
        status = status + chr(8)*(len(status)+1)
        label = pyxbmct.Label(status, alignment=pyxbmct.ALIGN_CENTER)
