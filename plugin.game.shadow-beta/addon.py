import xbmcplugin
import xbmcgui
import xbmcaddon
import xbmcvfs

import os
import subprocess
import urllib
import json
from packaging import version

# TODO: Fix the download
# Download the AppImage
#
# @param String Version of the AppImage
# @param String Target path to download
def downloadAppImage(version, path):
    url = 'https://gitlab.com/aar642/shadow-appimage/-/jobs/artifacts/%s/download?job=app_build' % version
    url = 'https://gitlab.com/aar642/shadow-appimage/-/jobs/93590232/artifacts/download'

    progress_bar = xbmcgui.DialogProgressBG()
    progress_bar.create('Download AppImage', '%s' % version)

    def reporthook(block_number, block_size, total_size):
        if 0 != block_number & 511:
             percent = (block_number * block_size * 100) / total_size
             progress_bar.update(percent)

    urllib.urlretrieve(url, '%s.zip' % path, reporthook)
    progress_bar.close()

    os.system('unzip "%s.zip" -d "%s" && chmod +x "%s" && rm "%s.zip"' % (path, appImageFolder, path, path))

# Get the information about the latest release
#
# @return Array Information
def getInfoLatestRelease():
    response = urllib.urlopen('https://gitlab.com/api/v4/projects/7962701/repository/tags')
    data = json.loads(response.read())

    i = 0
    while not (data[i]['release']):
        i = i + 1

    return data[i]



# Get informations about the addon
addon               = xbmcaddon.Addon()
addonID             = addon.getAddonInfo('id')
addonFolder         = xbmc.translatePath('special://home/addons/' + addonID).decode('utf-8')

icon                = os.path.join(addonFolder, "resources/icon.png")#.encode('utf-8')
version             = getInfoLatestRelease()['release']['tag_name']
speeds              = [5, 10, 15, 20, 30, 40, 50]

appImageFolder      = os.path.abspath('.local/share/Shadow Beta')
os.system('mkdir -p "%s"' % appImageFolder)
pathAppImage        = os.path.join(appImageFolder, "shadowbeta-linux-x86_64.AppImage")


# Store the settings
settings = {
    "username":         addon.getSetting("username"),
    "password":         addon.getSetting("password"),

    "autoBandwidth":    (addon.getSetting("autoBandwidth") == 'true'),
    "bandwidth":        speeds[ int(addon.getSetting("bandwidth")) ],

    "hevc":             addon.getSetting("hevc")
}


# Download the AppImage
if not os.path.exists( pathAppImage ):
    downloadAppImage(version, pathAppImage)

else:
    lVersion = subprocess.check_output([pathAppImage, '--version'])
    xbmc.log('########################### %s' % lVersion)

    # Check if the version is conform
    if( lVersion[0] == 'v' ):

        xbmc.log('########################### Version conforme')

        # Update available
        if( version.parse(lVersion) < version.parse(version) ):
            downloadAppImage(version, pathAppImage)


# Display information about the configuration as a Notification
if settings['autoBandwidth']:
    bandwidth = 'Automatic'
else:
    bandwidth = '%d MBits/s' % settings['bandwidth']

if settings['hevc']:
    encode = 'H265'
else:
    encode = 'H264'

xbmc.executebuiltin('Notification(Shadow,Log as %s\n%s - %s, 5000, %s)' % (settings['username'], bandwidth, encode, icon))


# Start the AppImage
# os.system(pathAppImage)
